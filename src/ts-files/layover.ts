const menuBtn: HTMLElement | null = document.querySelector("#toggle-menu");
const searchBtn: HTMLElement | null = document.querySelector("#search");
const filterBtn: HTMLElement | null = document.querySelector("#toggle-filters");
const menuOverlay: HTMLElement | null = document.querySelector(".menu-overlay");
const searchOverlay: HTMLElement | null = document.querySelector(".search-overlay");
const filtersOverlay: HTMLElement | null = document.querySelector(".filters-overlay");
const closeMenu: HTMLElement | null = document.querySelector("#close-menu");
const closeSearch: HTMLElement | null = document.querySelector("#close-search");
const closeFilters: HTMLElement | null = document.querySelector("#close-filters");

export const overlayAction = () => {
    if (menuBtn && menuOverlay) {
        menuBtn.addEventListener("click", function () {
            this.classList.toggle("open");
            menuOverlay.classList.toggle("open");
        });
    }

    if (closeMenu && menuOverlay) {
        closeMenu.addEventListener("click", function () {
            this.classList.toggle("open");
            menuOverlay.classList.toggle("open");
        });
    }

    if (searchBtn && searchOverlay) {
        searchBtn.addEventListener("click", function () {
            this.classList.toggle("open");
            searchOverlay.classList.toggle("open");
        });
    }

    if (closeSearch && searchOverlay) {
        closeSearch.addEventListener("click", function () {
            this.classList.toggle("open");
            searchOverlay.classList.toggle("open");
        });
    }

    if (filterBtn && filtersOverlay) {
        filterBtn.addEventListener("click", function () {
            this.classList.toggle("open");
            filtersOverlay.classList.toggle("open");
        });
    }

    if (closeFilters && filtersOverlay) {
        closeFilters.addEventListener("click", function () {
            this.classList.toggle("open");
            filtersOverlay.classList.toggle("open");
        });
    }
};
