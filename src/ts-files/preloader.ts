export const preLoader = () => {
    const preloader = document.querySelector('#preloader');

    if (preloader) {
        window.addEventListener('load', () => {
            setTimeout(() => {
                preloader.classList.add('loaded');
            }, 500);
            setTimeout(() => {
                preloader.remove();
            }, 2000);
        });
    }
}
