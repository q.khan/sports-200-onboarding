// In this function we are fetching data from api and passing it to the function which display a product

import { euroGerman } from "./formatPrice";

interface product {
    id: number;
    brand: string;
    category: string;
    description: string;
    discountPercentage: number;
    price: number;
    rating: number;
    stock: string;
    thumbnail: string;
    title: string;
}

const url = "https://dummyjson.com/products";

export const getProductsData = async () => {
    let productsContainer: HTMLElement | null = document.getElementById("product-count");
    const productsCount = document.createElement("p");

    try {
        // We are using fetch to get the response
        const response = await fetch(url);
        const data = await response.json();
        let productsData = data.products;
        const productsCountTextNode = document.createTextNode(productsData.length + " Products");
        productsCount.appendChild(productsCountTextNode);
        productsContainer ? productsContainer.appendChild(productsCount) : null;

        productsData.forEach((product: product,index:number) => {
            if(index < productsData.length - 1)
            {
                console.log(index)
                listData(product);
            }
        });
        // Trigger the listData function and pass the result
    } catch (error) {
        console.log(error);
    }
};

// This function will get single product and will create dynamic dom elements and insert the dom element according to the design
const listData = (product: product) => {
    // parent Node inside which products will be inserted
    let productsContainer: HTMLElement | null = document.getElementById("products-container");
    // Box for a single product
    const productBoxCol = document.createElement("div");
    productBoxCol.className = "col-6 product-box";
    productsContainer ? productsContainer.appendChild(productBoxCol) : null;

    // Card for displaying the data
    const productBox = document.createElement("div");
    productBox.className = "card position-relative product-card";
    productBoxCol.appendChild(productBox);

    // Card Badge Element
    const neuBadge = document.createElement("span");
    neuBadge.className = "new-product-badge";
    const badgeNode = document.createTextNode("Neu");
    neuBadge.appendChild(badgeNode);
    productBox.appendChild(neuBadge);

    // Heart Icon

    const heartIcon = document.createElement("i");
    heartIcon.className = "bi bi-heart d-block d-lg-none d-xl-none d-xxl-none heart-icon";
    productBox.appendChild(heartIcon);

    heartIcon.addEventListener("mouseover", () => {
        heartIcon.className = "bi-heart-fill heart-icon";
    });

    heartIcon.addEventListener("mouseout", () => {
        heartIcon.className = "bi-heart heart-icon";
    });

    heartIcon.addEventListener("focus", () => {
        heartIcon.className = "bi-heart-fill heart-icon";
    });

    // Card Product Image
    const productImg = document.createElement("img");
    productImg.src = '/assets/images/dolomite.jpeg';
    productImg.alt = product.title;
    productImg.className = "card-img-top px-2";
    productBox.appendChild(productImg);

    // Card Body which will contain Product Information
    const cardBody = document.createElement("div");
    cardBody.className = "card-body";
    productBox.appendChild(cardBody);

    const productBrandName = document.createElement("h5");
    productBrandName.className = "brand-name";
    const productBrandTextNode = document.createTextNode(product.brand);
    productBrandName.appendChild(productBrandTextNode);
    cardBody.appendChild(productBrandName);

    const productName = document.createElement("p");
    productName.className = "product-name";
    const productTextNode = document.createTextNode(product?.title);
    productName.appendChild(productTextNode);
    cardBody.appendChild(productName);

    const productCategoryName = document.createElement("p");
    productCategoryName.className = "category-name";
    const productCategoryTextNode = document.createTextNode(product?.brand);
    productCategoryName.appendChild(productCategoryTextNode);
    cardBody.appendChild(productCategoryName);

    const productPrice = document.createElement("p");
    productPrice.className = "product-price";
    const productPriceTextNode = document.createTextNode(euroGerman.format(product?.price));
    productPrice.appendChild(productPriceTextNode);
    cardBody.appendChild(productPrice);

    const productQuantity = document.createElement("p");
    productQuantity.className = "product-quantity";
    const productQuantityTextNode = document.createTextNode(`+${product?.stock}  Farben`);
    productQuantity.appendChild(productQuantityTextNode);
    cardBody.appendChild(productQuantity);
};
