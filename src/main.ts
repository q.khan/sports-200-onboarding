import '../scss/style.scss';
import { preLoader } from "./ts-files/preloader";
import { overlayAction } from './ts-files/layover'
import { getProductsData } from "./ts-files/getProductsData";
import '../node_modules/bootstrap/dist/js/bootstrap.js';

// function call for preloader function
preLoader();
// call for the actions if someone clicks the menu icons or filters or search
overlayAction();
// call to get the products data
await getProductsData();
